package com.maizi.oracle;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

/**
 * @author
 * @since 2020/12/21
 */
public class OracleCodeGenerator2 {

    @Test
    public void run() {

        // 1、创建代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 2、全局配置
        // 2、全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        //todo 修改一：将这修改为绝对路径，代码生成后输出的目录
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("xuzhaoyi"); //代码生成器的作者  todo 可选
        gc.setOpen(false); //生成后是否打开资源管理器
        gc.setFileOverride(false); //重新生成时文件是否覆盖
        gc.setServiceName("%sService");    //去掉Service接口的首字母I
        //todo 修改二：主键生成的类型,数据库主键类型char 还是int决定
        gc.setIdType(IdType.ID_WORKER_STR); //主键策略，
        gc.setDateType(DateType.ONLY_DATE);//定义生成的实体类中日期类型
        //todo 修改三：是否开启Swagger2
        gc.setSwagger2(false);//开启Swagger2模式

        mpg.setGlobalConfig(gc);

        // 3、数据源配置  todo:修改四：修改数据库相关设置
        DataSourceConfig dsc = new DataSourceConfig();
        //dsc.setUrl("jdbc:mysql://localhost:3306/guli?serverTimezone=GMT%2B8");
        dsc.setUrl("jdbc:oracle:thin:@127.0.0.1:1521:orcl");
        //dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setDriverName("oracle.jdbc.driver.OracleDriver");
//        dsc.setUsername("root");
//        dsc.setPassword("root");
        dsc.setUsername("scott");
        dsc.setPassword("scott");
        //dsc.setDbType(DbType.MYSQL);
        dsc.setDbType(DbType.ORACLE);
        mpg.setDataSource(dsc);

        // 4、包配置 todo 修改五：包名
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.neusoft.venus.modular.costquota");
        // pc.setModuleName("edu"); //模块名
        //com.guli.deu.controller
        pc.setController("controller");
        //com.guli.deu.entity
        pc.setEntity("entity");
        //com.guli.deu.service
        pc.setService("service");
        //com.guli.deu.mapper
        pc.setMapper("dao");
        mpg.setPackageInfo(pc);

        // 5、策略配置
        StrategyConfig strategy = new StrategyConfig();
        //todo 修改六：生成的表名，多个用逗号分隔
        strategy.setInclude("CBDE_SYSPARA");
        strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
        strategy.setTablePrefix(pc.getModuleName() + "_"); //生成实体时去掉表前缀

        strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
        strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作
        strategy.setRestControllerStyle(true); //restful api风格控制器
        strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符

        mpg.setStrategy(strategy);

        // 6、执行
        mpg.execute();
    }
}